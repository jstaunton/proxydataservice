package ie.proxyData;

import java.util.concurrent.TimeUnit;

import org.apache.tomcat.jni.Time;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ie.proxyData.influxdb.InfluxDBTemplate;

@SpringBootApplication
public class ProxyDataServiceApplication implements CommandLineRunner{

	private static Logger logger = LoggerFactory.getLogger(ProxyDataServiceApplication.class);
	
	@Autowired
	private InfluxDBTemplate<Point> influxDBTemplate;
	
	
	@Override
	public void run(final String... args) throws Exception {
		
		// define the range 
        int tempMax = 30; 
        int tempMin = 20; 
        int tempRange = tempMax - tempMin + 1;
        
        int humidMax = 90; 
        int humidMin = 80; 
        int humidRange = humidMax - humidMin + 1;
        
	    // Create database...
	    influxDBTemplate.createDatabase();
		logger.info("VERSION ---------------------" + influxDBTemplate.version());
		int counter = 0;
		while(counter < 1000) {
			Thread.sleep(30000);
			// Create some data...
			System.out.println("***********");
			int tempRand = (int)(Math.random() * tempRange) + tempMin; 
			int humidRand = (int)(Math.random() * humidRange) + humidMin;
		    final Point p2 = Point.measurement("sensor1")
		    		.tag("building", "block_A")
		    		.tag("room", "b219")
		      .addField("temperature", tempRand)
		      .addField("humidity", humidRand)
		      .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
		      .build();
		    
		    final Point p4 = Point.measurement("sensor2")
		    		.tag("building", "block_B")
		    		.tag("room", "c222")
		      .addField("temperature", tempRand - 10)
		      .addField("humidity", humidRand -10)
		      .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
		      .build();
	//	    influxDBTemplate.write(p1, p2, p3, p4);
		    influxDBTemplate.write(p2, p4);
		    counter++;

	    // ... and query the latest data
//	    final Query q = new Query("SELECT * FROM cpu GROUP BY tenant", influxDBTemplate.getDatabase());
//	    influxDBTemplate.query(q, 10, queryResult -> logger.info(queryResult.toString()));
	    
//	    final Query q1 = new Query("SELECT * FROM disk GROUP BY tenant", influxDBTemplate.getDatabase());
//	    influxDBTemplate.query(q1, 10, queryResult -> logger.info(queryResult.toString()));
		  
//	    final Query q3 = new Query("SELECT humidity FROM sensor1 WHERE room = 'b219'", influxDBTemplate.getDatabase());
//	    influxDBTemplate.query(q3, 5, queryResult -> logger.info(queryResult.toString()));
//	    logger.info("------------------------------------------");
//	    final Query q4 = new Query("SELECT * FROM sensor1", influxDBTemplate.getDatabase());
//	    influxDBTemplate.query(q4, 5, queryResult -> logger.info(queryResult.toString()));
		  
	    //change db to newDb for this query
//	    final Query q2 = new Query("SELECT * FROM temperature GROUP BY room", influxDBTemplate.getDatabase());
//	    influxDBTemplate.query(q2, 10, queryResult -> logger.info(queryResult.toString()));
	    
	    //Time.sleep(10000);
		}
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ProxyDataServiceApplication.class, args);
	}

}
