package ie.proxyData.services;

import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.proxyData.influxdb.InfluxDBTemplate;

@Service
public class DataServiceImpl implements DataService{

private static Logger logger = LoggerFactory.getLogger(DataServiceImpl.class);
	
	@Autowired
	private InfluxDBTemplate<Point> influxDBTemplate;
	
	@Override
	public Query getSensorData() {
		final Query query = new Query("SELECT humidity FROM sensor1 WHERE room = 'b219'", influxDBTemplate.getDatabase());
	    influxDBTemplate.query(query, 5, queryResult -> logger.info(queryResult.toString()));
		return query;
	}

}
