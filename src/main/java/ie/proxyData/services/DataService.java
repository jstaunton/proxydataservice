package ie.proxyData.services;

import org.influxdb.dto.Query;

public interface DataService {

	Query getSensorData();
}
