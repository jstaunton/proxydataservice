package ie.proxyData.controllers.restApi;


import org.influxdb.dto.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ie.proxyData.services.DataService;

@RestController	
@RequestMapping("/api")
public class RestControllerApi {
	private static Logger logger = LoggerFactory.getLogger(RestControllerApi.class);
	@Autowired
	DataService dataService;
	
	@GetMapping("/sensorData")
	public Query getSensorData(){
	
		Query allData = dataService.getSensorData();
		logger.info("$$$$$$$$$$$$$$$$$$$$ inApi" + allData.toString());
		
		return allData;
	}
}
